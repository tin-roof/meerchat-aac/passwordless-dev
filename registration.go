package passwordless

import "context"

// Registration defines the structure of the registration request
type Registration struct {
	Aliases     []string `json:"aliases,omitempty"`
	DisplayName string   `json:"displayname,omitempty"`
	UserID      string   `json:"userId,omitempty"`
	Username    string   `json:"username,omitempty"`
}

// resistrationResponse response structure from the registration endpoint
type resistrationResponse struct {
	Token string `json:"token"`
}

// Register registers a new user and return the registration token
func (c *Client) Register(reg Registration) (string, error) {
	return c.RegisterWithContext(context.Background(), reg)
}

// RegisterWithContext registers a new user with a context and return the registration token
func (c *Client) RegisterWithContext(ctx context.Context, reg Registration) (string, error) {
	var res resistrationResponse
	if err := c.request(ctx, RegisterRoute, reg, &res); err != nil {
		return "", err
	}

	return res.Token, nil
}
