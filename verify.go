package passwordless

import (
	"context"
	"time"

	"github.com/google/uuid"
)

// Credentials defines the structure of the credentials returned from the passwordless.dev API
type Credentials struct {
	Country   string    `json:"country"`
	Device    string    `json:"device"`
	ExpiresAt time.Time `json:"expiresAt"`
	ID        string    `json:"credentialId"`
	Nickname  string    `json:"nickname"`
	Origin    string    `json:"origin"`
	RPID      string    `json:"rpid"`
	Success   bool      `json:"success"`
	Timestamp time.Time `json:"timestamp"`
	Type      string    `json:"type"`
	UserID    uuid.UUID `json:"userId"`
}

// verifyRequst
type verifyRequest struct {
	Token string `json:"token"`
}

// Verify uses a token to log a user in and returns the credentials
func (c *Client) Verify(token string) (Credentials, error) {
	return c.VerifyWithContext(context.Background(), token)
}

// VerifyWithContext logs a user in with a context and return the user credentials
func (c *Client) VerifyWithContext(ctx context.Context, token string) (Credentials, error) {
	var output Credentials
	if err := c.request(ctx, VerifyRoute, verifyRequest{token}, &output); err != nil {
		return output, err
	}

	return output, nil
}
