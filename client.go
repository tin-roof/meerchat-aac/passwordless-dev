package passwordless

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
)

// Version used to define the available version of the passwordless.dev API
type Version string

const (
	V4 Version = "v4"
)

// versionURL is a map of the API version to the corresponding URL
var versionURL map[Version]string = map[Version]string{
	V4: "https://v4.passwordless.dev",
}

// route defines the available routes for the passwordless.dev API
type route string

const (
	VerifyRoute   route = "/signin/verify"
	RegisterRoute route = "/register/token"
)

// Client defines the passwordless.dev API client
type Client struct {
	secret  string
	version Version
}

// NewClient creates a new passwordless.dev client
func NewClient(version Version, secret string) *Client {
	return &Client{secret, version}
}

// request makes an http request to the designated endpoin
func (c *Client) request(ctx context.Context, endpoint route, input, output any) error {
	// setup the request body
	var body []byte
	if input != nil {
		// if the request is a string, just use it as the body else assume its a struct and marshal it
		// @TODO: this could be checked a bit better and handles other types accordingly
		if s, ok := input.(string); ok {
			body = []byte(s)
		} else {
			bytes, err := json.Marshal(input)
			if err != nil {
				return err
			}
			body = bytes
		}
	}

	// setup the API request
	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		versionURL[c.version]+string(endpoint),
		bytes.NewBuffer(body),
	)
	if err != nil {
		return err
	}

	// add headers
	req.Header.Add("ApiSecret", c.secret)
	req.Header.Add("Content-Type", "application/json")

	// make the request to the API
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	// @TODO: check some response codes and things

	// read the response body
	body, err = io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	// upack the response into the response struct
	err = json.Unmarshal(body, output)
	if err != nil {
		return err
	}

	return nil
}
