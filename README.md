# Passwordless-dev

Go client package to work with Bitwarden's passwordless.dev for passwordless auth and registration.

# Usage

Using the Passwordless PKG is pretty striaght forward.

## Client

Creating a client is simple, you just need to select the API version and pass in your secret key

```go
client := passwordless.NewClient(passwordless.V4, "bitwarden-passwordless-secret")
```

### Supported Versions

- V4 of the API

## Registration

Create a new registration in the Bitwarden API using details gathered during registration

```go
// create a registration object
reg := passwordless.Registration{
    Aliases:     []string{"test@test.com"},
    DisplayName: "Test Guy",
    Username:    "test@test.com",
    UserID:      "some-unique-user-id-value",
}

// send the resgistration
token, err := client.Register(reg)
if err != nil {
    // @TODO: handle the failed registration
}

// @TODO: pass the token back to the front end
```

## Verification

Verify the token received during the login process

```go
token := "auth-token-from-front-end"

// verify the token
credentials, err := client.Verify(token)
if err != nil {
    return nil, err
}

// check to make sure the login was successful
if !credentials.Success {
    // @TODO: handle the failed login
}

// @TODO: handle successful login
```
